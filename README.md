CoreOS Cluster Builder
======================

Run with `./cluster.sh -s 3`

Requirements
============

* `doctl`               : https://github.com/digitalocean/doctl
* `cfssl` / `cfssljson` : https://pkg.cfssl.org/
* `mo`                  : https://github.com/tests-always-included/mo
* `jq`                  : https://stedolan.github.io/jq/

Most of these are available on macOS via [homebrew](http://brew.io)

What happens behind the scenes?
===============================

* A *discovery url* is obtained from https://discovery.etcd.io
* `doctl` is leveraged to generate prefixed instances
* A certificate authority is generated
* Each provisioned machine gets a client/server certificate to enable secure communication for fleet/etcd/docker, etc.
