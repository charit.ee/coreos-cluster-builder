#!/bin/bash
set -e

BASE_PATH=$(pwd -P)
CLUSTER_SIZE=${CLUSTER_SIZE=3}
REGION=${REGION=tor1}
DROPLET_SIZE=${DROPLET_SIZE=512mb}
PREFIX=${PREFIX=coreos}


export DOMAIN="charitee.local"
export CA_COUNTRY="CA"
export CA_STATE="BC"
export CA_LOCALITY="Vancouver"
export CA_ORGANIZATION="Charit.ee Inc."
export CA_OU="Operations"
export CERT_PATH="/etc/pki/coreos"
export FLANNEL_NETWORK="10.1.0.0/16"

function binary_check {
    echo "Checking for '$1'"
    type "$1" >/dev/null 2>&1 || { echo >&2 "Please install '$1' ($2) before running"; exit 1; }
}

function show_help {
    echo "Usage $0 [-s 3]"
    echo "  -s Cluster size (3)"
    echo "  -d Droplet size (512mb)"
    echo "  -p Node name prefix (coreos)"
    echo "  -r Region (tor1)"
    echo
}

echo "Parsing command line params"
OPTIND=1
while getopts "h\?s:p:r:d:" opt; do
case "${opt}" in
    h|\?)
        show_help
        exit 0
        ;;
    p)
        DROPLET_SIZE=$OPTARG
        ;;
    r)
        REGION=$OPTARG
        ;;
    p)
        PREFIX=$OPTARG
        ;;
    s)
        CLUSTER_SIZE=$OPTARG
        ;;
esac
done
shift $((OPTIND-1))

function get_private_ip {
    echo "Getting private IP for ${1}"
    private_ip=$(doctl compute droplet get "$1" -o json | jq -r '.[].networks.v4[]|select(.type=="private").ip_address')
}

function get_discovery_url {
    echo "Generating discovery URL for ${CLUSTER_SIZE}-node cluster"
    DISCOVERY_URL=$(curl -s "https://discovery.etcd.io/new?clusterSize=${CLUSTER_SIZE}")
    echo "Discovery URL: ${DISCOVERY_URL}"
    export DISCOVERY_URL
}

function make_ca {
    ca_path="${BASE_PATH}/ca"
    if [ ! -d "$ca_path" ]
    then
        echo "Certificate authority is not configured"
        mkdir -p "$ca_path"
        pushd "$ca_path" >/dev/null
            export CA_CSR_CN="Charit.ee CA"
            export CA_CSR_HOST="charit.ee"

            cfssl gencert -initca <(mo "$BASE_PATH/templates/ca-csr.json.template") 2>/dev/null | cfssljson -bare ca -
        popd
    fi
}

function make_client_cert {
    local ssh_params=(-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null)
    ca_path="${BASE_PATH}/ca"
    if [ -d "$ca_path" ]
    then
        tempdir=$(mktemp -d -t cfssl)
        pushd "$tempdir" >/dev/null
            export CLIENT_CN="$machine"
            export CLIENT_PRIVATE_IPV4="$private_ip"
            export CLIENT_PUBLIC_IPV4="$public_ip"
            export CA_EXPIRY=${CA_EXPIRY=43800h}

            echo "Generating client certificate for ${machine}"
            cfssl gencert \
                -ca="$BASE_PATH/ca/ca.pem" \
                -ca-key="$BASE_PATH/ca/ca-key.pem" \
                -profile=client-server \
                -config=<(mo "$BASE_PATH/templates/ca-config.json.template") \
                <(mo "$BASE_PATH/templates/client-csr.json.template") 2>/dev/null | cfssljson -bare coreos
            chmod 0644 coreos-key.pem
            # shellcheck disable=SC2029
            ssh "${ssh_params[@]}" "core@${public_ip}" "sudo mkdir -p $CERT_PATH && sudo chown root:core $CERT_PATH && sudo chmod g+w $CERT_PATH"
            scp "${ssh_params[@]}" "$BASE_PATH/ca/ca.pem" coreos-key.pem coreos.pem "core@${public_ip}:${CERT_PATH}"
            ssh "${ssh_params[@]}" "core@${public_ip}" 'sudo reboot' || true
        popd
        rm -rf "$tempdir"
    else
        echo "Certificate Authority is not initialized"; exit 1;
    fi
}

binary_check "jq" "https://stedolan.github.io/jq/"
binary_check "cfssl" "https://pkg.cfssl.org/"
binary_check "cfssljson" "https://pkg.cfssl.org/"
binary_check "mo" "https://raw.githubusercontent.com/tests-always-included/mo/master/mo"
binary_check "doctl" "https://github.com/digitalocean/doctl"
get_discovery_url
make_ca

# create n machines
for sequence in $(seq -w 1 "${CLUSTER_SIZE}" );
do
    fingerprint=$(ssh-keygen -lf ~/.ssh/id_rsa.pub | awk '{print $2}')
    machine="${PREFIX}-${sequence}"
    echo "Building ${machine} machine"
    echo "Allowing SSH Fingerprint ${fingerprint}"
    result=$(doctl compute droplet create "${machine}" --no-header --wait --enable-private-networking --image coreos-stable --size "${DROPLET_SIZE}" --region "${REGION}" --ssh-keys "${fingerprint}" --user-data-file <(mo "$BASE_PATH/templates/cloud-config.yml.template") --format "ID,PublicIPv4")
    machine_id=$(echo "$result" | awk '{print $1}')
    get_private_ip "$machine_id"
    public_ip=$(echo "$result" | awk '{print $2}')
    printf "Machine provisioned.\nPrivate IP: %s\nPublic IP : %s\n" "${private_ip}" "${public_ip}"

    make_client_cert

done
